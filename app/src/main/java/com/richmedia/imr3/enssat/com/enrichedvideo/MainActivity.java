package com.richmedia.imr3.enssat.com.enrichedvideo;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private LinkedHashMap<Integer,String> mMetas;
    private Handler mHandler;
    private static final int UPDATE_FREQ = 500;
    private VideoView mVideoView;
    private Uri mVideoUri;
    private WebView mWebView;
    private MapView mMapView;
    private String MAPVIEW_BUNDLE_KEY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();
        initVideo();
        initWikipedia();

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mMapView = findViewById(R.id.mapview);
        mMapView.onCreate(mapViewBundle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVideoView.setVideoURI(mVideoUri);
        mVideoView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVideoView.pause();
    }


    private void initVideo(){
        mVideoView = (VideoView)findViewById(R.id.videoview);
        try {
            MediaController mediacontroller = new MediaController(this);
            mediacontroller.setAnchorView(mVideoView);
            mVideoUri = Uri.parse(getString(R.string.video_url));
            mVideoView.setMediaController(mediacontroller);
            mVideoView.setVideoURI(mVideoUri);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        mVideoView.requestFocus();
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mVideoView.start();
                initChapters();
            }
        });
        //fullscreen();
    }



    private void initWikipedia() {
        mWebView = (WebView)findViewById(R.id.wikipedia);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.setWebViewClient(new WebViewClient() {


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        mWebView.loadUrl(getString(R.string.wikipedia_url));
    }
    private void fullscreen() {
        DisplayMetrics metrics = new DisplayMetrics(); getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) mVideoView.getLayoutParams();
        params.width =  metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        mVideoView.setLayoutParams(params);
    }

    private View.OnClickListener chaptersListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int position = (int)v.getTag()*1000;
            mVideoView.seekTo(position);
        }
    };

    private void initChapters(){
        InputStream inputStream = getResources().openRawResource(R.raw.chapters);
        JSONArray jArray = null;
        try {
            parserJson chap = new parserJson(inputStream,"Chapters");
            jArray = chap.getList();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        int pos = 0;
            String title = "";
            LinearLayout chapters = (LinearLayout)findViewById(R.id.chapters);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            for (int i = 0; i < jArray.length(); i++) {
                try {
                    pos = jArray.getJSONObject(i).getInt("pos");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    title = jArray.getJSONObject(i).getString("title");
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                Button button = new Button(this);
                button.setTag(pos);
                button.setText(title);
                button.setLayoutParams(layoutParams);
                button.setOnClickListener(chaptersListener);
                chapters.addView(button);
            }
        }




    private void parseMetas(){
        InputStream inputStream = getResources().openRawResource(R.raw.timestamps);
        JSONArray jArray = null;
        try {
            parserJson stamp = new parserJson(inputStream,"Timestamps");
            jArray = stamp.getList();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

            int pos = 0;
            String url = "";
            mMetas = new LinkedHashMap<>();
            for (int i = 0; i < jArray.length(); i++) {
                try {
                    pos = jArray.getJSONObject(i).getInt("pos");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    url = jArray.getJSONObject(i).getString("url");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mMetas.put(pos,url);
            }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }
        mMapView.onSaveInstanceState(mapViewBundle);
    }

    private String getClosestUrl(int position){
        String result = null;
        for(Map.Entry<Integer,String> meta : mMetas.entrySet()){
            if(meta.getKey()<position){
                result = meta.getValue();
            };
            if(meta.getKey()>position){
                break;
            }
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void initMap(){
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                try {
                    long lat = 0;
                    long lng = 0;
                    String label = "";
                    int timestamp = 0;

                    InputStream inputStream = getResources().openRawResource(R.raw.chapters);
                    JSONArray mWaypoints = null;
                    try {
                        parserJson stamp = new parserJson(inputStream,"Waypoints");
                        mWaypoints = stamp.getList();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    for (int i = 0; i < mWaypoints.length(); i++) {
                        lat = mWaypoints.getJSONObject(i).getLong("lat");
                        lng = mWaypoints.getJSONObject(i).getLong("lng");
                        label = mWaypoints.getJSONObject(i).getString("label");
                        timestamp = mWaypoints.getJSONObject(i).getInt("timestamp");
                        Marker marker = googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat,lng))
                                .title(label));
                        marker.setTag(timestamp);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        int timestamp = (int)marker.getTag();
                        mVideoView.seekTo(timestamp * 1000);
                        return false;
                    }
                });
            }
        });
    }
}

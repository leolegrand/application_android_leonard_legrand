package com.richmedia.imr3.enssat.com.enrichedvideo;

import org.json.JSONArray;

import java.io.InputStream;

        import android.view.ViewGroup;
        import android.widget.Button;
        import android.widget.LinearLayout;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.ByteArrayOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.util.LinkedHashMap;


public class parserJson {

    private InputStream mInputStream;
    private JSONArray jArray;
    private String nom;

    public parserJson(InputStream is, String n) throws JSONException, IOException {

        this.nom =n;
        this.mInputStream = is;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;

        try {
            ctr = mInputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = mInputStream.read();
            }
            mInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            JSONObject jObject = new JSONObject(byteArrayOutputStream.toString());
            this.jArray = jObject.getJSONArray(this.nom);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONArray getList(){
        return this.jArray;
    }
}